package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"gitlab.com/EleMint/gsm/sdk"
)

// HealthGroupHandler - Initializes Health Group routes
func HealthGroupHandler(conn *sdk.SerialConn, rg *gin.RouterGroup) {
	rg.GET("", HealthCheck(conn))
}

// HealthCheck - Check connection to GSM Modem
func HealthCheck(conn *sdk.SerialConn) func(*gin.Context) {
	return func(c *gin.Context) {
		ok, err := conn.HealthCheck()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"service": http.StatusOK,
				"modem":   http.StatusServiceUnavailable,
				"error":   errors.Unwrap(err),
			})
			return
		}

		if !ok {
			c.JSON(http.StatusInternalServerError, gin.H{
				"service": http.StatusOK,
				"modem":   http.StatusServiceUnavailable,
				"error":   "modem responded with a failed status",
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"service": http.StatusOK,
			"modem":   http.StatusOK,
			"error":   nil,
		})
	}
}
