package controllers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"gitlab.com/EleMint/gsm/sdk"
)

// SMSGroupHandler - Initializes SMS Group routes
func SMSGroupHandler(conn *sdk.SerialConn, rg *gin.RouterGroup) {
	rg.POST("", PostSMS(conn))
	rg.GET("", GetSMSByMessageType(conn))
	rg.GET("/:index", GetSMS(conn))
}

// PostContract struct
type PostContract struct {
	To      string `json:"to"`
	Message string `json:"message"`
	IsDraft bool   `json:"isDraft"`
}

// PostSMS - Saves/Sends an SMS Message
func PostSMS(conn *sdk.SerialConn) func(c *gin.Context) {
	return func(c *gin.Context) {
		var cont PostContract

		err := c.BindJSON(&cont)
		if err != nil {
			c.String(http.StatusBadRequest, "%v", errors.Unwrap(err))
			return
		}

		idx, err := conn.StoreSMS(cont.To, cont.Message)
		if err != nil {
			c.String(http.StatusInternalServerError, "%v", errors.Unwrap(err))
			return
		}
		c.String(http.StatusOK, "message saved")

		if !cont.IsDraft {
			err = conn.SendStoredSMS(idx)
			if err != nil {
				c.String(http.StatusInternalServerError, "%v", errors.Unwrap(err))
				return
			}

			c.String(http.StatusOK, "message sent")
			return
		}
	}
}

// GetSMS - Gets an SMS Message from Storage
func GetSMS(conn *sdk.SerialConn) func(*gin.Context) {
	return func(c *gin.Context) {
		msgIdx := c.Param("index")
		idx, err := strconv.Atoi(msgIdx)
		if err != nil {
			c.String(http.StatusBadRequest, "%v", err)
		}

		m, err := conn.GetStoredSMS(idx)
		if err != nil {
			c.String(http.StatusInternalServerError, "%v", errors.Unwrap(err))
			return
		}

		c.JSON(http.StatusOK, m)
	}
}

// GetSMSByMessageType - Gets a list of SMS Messages from Storage
func GetSMSByMessageType(conn *sdk.SerialConn) func(*gin.Context) {
	return func(c *gin.Context) {
		msgType := c.Query("type")
		t, err := sdk.ParseMessageType(msgType)
		if err != nil {
			c.String(http.StatusBadRequest, "%v", errors.Unwrap(err))
			return
		}

		msgs, err := conn.ListStoredSMS(t)
		if err != nil {
			c.String(http.StatusInternalServerError, "%v", errors.Unwrap(err))
			return
		}

		c.JSON(http.StatusOK, msgs)
	}
}
