package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/EleMint/gsm/sdk"
	"gitlab.com/EleMint/gsm/service/http/controllers"
)

var host string     // Server Host
var hostname string // Server Hostname (Host:Port)
var port string     // Server Port

var spName string        // Serial Port Name
var spBaud int           // Serial Port Baud Rate
var spTime time.Duration // Serial Port ReadTimeout (in ms)
var spSize int           // Serial Port Size of Data Bits
var spPari string        // Serial Port Parity Bit Type
var spStop int           // Serial Port Number of Stop Bits

var conn *sdk.SerialConn // Serial Port Connection
var ngin *gin.Engine     // Service routing engine

func init() {
	initFlags()
}

func main() {
	log.Printf("Opening serial port connection (%s)...\n", spName)
	conn = openSerialPort()
	defer conn.Close(func(name string) {
		log.Printf("Serial port disconnected (%s)\n", name)
	})

	ngin = initRoutes()

	log.Printf("Starting service on %s...\n", hostname)
	ngin.Run()
}

func initRoutes() *gin.Engine {
	log.Println("Initializing API...")
	defer log.Println("API Initialized")

	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	v1 := r.Group("v1")
	controllers.HealthGroupHandler(conn, v1.Group("/health"))
	controllers.SMSGroupHandler(conn, v1.Group("/sms"))

	return r
}

func openSerialPort() *sdk.SerialConn {
	conn, err := sdk.Open(
		spName,
		spBaud,
		spTime,
		spSize,
		spPari,
		spStop,
	)
	if err != nil {
		log.Fatalf("Failed to open serial port connection (%s): %v\n", spName, errors.Unwrap(err))
	}
	log.Printf("Serial port connected (%s)\n", spName)

	err = conn.SetSMSMode(sdk.TextMode)
	if err != nil {
		log.Fatalf("Failed to set SMS mode to 'TextMode': %v\n", errors.Unwrap(err))
	}

	return conn
}

func initFlags() {
	flag.StringVar(&host, "host", "localhost", "Server host")
	port = os.Getenv("PORT")
	if port == "" {
		os.Setenv("PORT", "8080")
		port = "8080"
	}
	hostname = fmt.Sprintf("%s:%s", host, port)

	flag.StringVar(&spName, "name", "/dev/tty.wchusbserial14320", "Serial Port Name")
	flag.IntVar(&spBaud, "baud", 115200, "Serial Port Baud Rate")
	flag.DurationVar(&spTime, "time", 500*time.Millisecond, "Serial Port ReadTimeout (in ms)")
	flag.IntVar(&spSize, "size", 8, "Serial Port Size of Data Bits")
	flag.StringVar(&spPari, "pari", "N", "Serial Port Parity Bit Type")
	flag.IntVar(&spStop, "stop", 1, "Serial Port Number of Stop Bits")
	flag.Parse()
}
