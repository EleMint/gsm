package sdk

import (
	"bytes"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/tarm/serial"
)

// SerialConn - A Serial Port Connection
type SerialConn struct {
	config *serial.Config
	port   *serial.Port
}

// Open - Opens a Serial Port Connection
func Open(name string, baud int, time time.Duration, size int, parity string, stop int) (*SerialConn, error) {
	pByte := byte(parity[0])
	var p serial.Parity
	p = serial.Parity(pByte)

	sByte := byte(stop)
	var s serial.StopBits
	s = serial.StopBits(sByte)

	conf := &serial.Config{
		Name:        name,
		Baud:        baud,
		ReadTimeout: time,
		Size:        byte(size),
		Parity:      p,
		StopBits:    s,
	}

	port, err := serial.OpenPort(conf)
	if err != nil {
		return nil, errors.Wrap(err, "sdk.Open - serial.OpenPort")
	}

	return &SerialConn{
		config: conf,
		port:   port,
	}, nil
}

// Close - Closes a Serial Port Connection.
// afterFn runs after calling connection.Close() - called with connection name.
func (conn *SerialConn) Close(afterFn func(string)) {
	conn.port.Close()
	afterFn(conn.config.Name)
}

const (
	// sigstop - Signal End of SMS Message
	sigstop = byte(26)

	nilPortErrorf    = "nil serial port: '%s'"
	commandError     = "command terminated with error"
	readErrorf       = "unable to read from serial port: '%s'"
	writeErrorf      = "unable to write to serial port: '%s'"
	flushErrorf      = "unable to flush serial port: '%s'"
	setSMSModeErrorf = "failed to set SMS Mode: '%s'"
)

// Exec - Write a command to the serial port connection and wait for response
func (conn *SerialConn) Exec(cmd Command, ct CommandType, arg string, waitForOK bool) (string, error) {
	if conn.port == nil {
		return "", errors.Errorf(nilPortErrorf, conn.config.Name)
	}

	err := conn.port.Flush()
	if err != nil {
		return "", errors.Wrapf(err, flushErrorf, conn.config.Name)
	}

	_, err = conn.write(cmd.String() + ct.String() + arg)
	if err != nil {
		return "", err
	}

	out, err := conn.read(true)
	if err != nil {
		return "", err
	}

	return out, nil
}

// HealthCheck - Used to check communication between Modem and Computer
func (conn *SerialConn) HealthCheck() (bool, error) {
	resp, err := conn.Exec(AT, Execute, "", true)
	if err != nil {
		return false, err
	}

	return isOK(resp), nil
}

// SetSMSMode - Set SMS Mode for Modem
func (conn *SerialConn) SetSMSMode(mode SMSMode) error {
	_, err := conn.Exec(CMGF, Set, mode.String(), true)
	if err != nil {
		return errors.Wrapf(err, setSMSModeErrorf, mode.Name())
	}
	return nil
}

// StoreSMS - Stores an SMS recipient and message
func (conn *SerialConn) StoreSMS(to, msg string) (int, error) {
	cmd := CMGW.String()
	if to != "" {
		cmd += "=\"" + to + "\"\n"
	}

	_, err := conn.write(cmd)
	if err != nil {
		return -1, err
	}

	_, err = conn.write(msg)
	if err != nil {
		return -1, err
	}

	_, err = conn.write(string(sigstop))
	if err != nil {
		return -1, err
	}

	out, err := conn.read(true)
	if err != nil {
		return -1, errors.Wrap(err, "failed to read from connection")
	}
	out = strings.TrimPrefix(out, "\r\n+CMGW: ")
	out = strings.TrimSuffix(out, "\r\n\r\nOK\r\n")

	msgID, err := strconv.Atoi(out)
	if err != nil {
		return -1, err
	}

	conn.port.Flush()

	return msgID, nil
}

// SendStoredSMS - Sends a message from storage
func (conn *SerialConn) SendStoredSMS(index int) error {
	if conn.port == nil {
		return errors.New("Serial Port - nil")
	}

	// get msg
	// send msg
	return nil
}

// GetStoredSMS - Retrieves a stored SMS message
func (conn *SerialConn) GetStoredSMS(index int) (*Message, error) {
	resp, err := conn.Exec(CMGR, Set, strconv.Itoa(index), true)
	if err != nil {
		return nil, err
	}

	lns := strings.Split(resp, "\r\n")
	meta := strings.TrimPrefix(lns[1], "+CMGR: ")
	metaFields := strings.Split(meta, ",")
	st := metaFields[0]
	stat, err := ParseMessageType(st)
	if err != nil {
		return nil, err
	}
	oa := metaFields[1]
	alpha := metaFields[2]
	scts := metaFields[3] + "-" + metaFields[4]
	data := lns[2 : len(lns)-3]

	msg := &Message{
		Index:                  index,
		Status:                 stat,
		OriginatorAddr:         oa,
		OriginatorName:         alpha,
		ServiceCenterTimeStamp: scts,
		Data:                   strings.Join(data, "\r\n"),
	}
	return msg, nil
}

// ListStoredSMS - Retrieves a list of SMS messages by message type
func (conn *SerialConn) ListStoredSMS(t MessageType) ([]*Message, error) {
	resp, err := conn.Exec(CMGL, Set, t.String(), true)
	if err != nil {
		return nil, err
	}

	msgs := strings.Split(resp, "+CMGL: ")
	for _, msg := range msgs[1:] {
		all := strings.Split(msg, "\r\n")
		meta := all[0]
		data := all[1]
		fmt.Println(meta)
		fmt.Println(data)
	}
	return nil, nil
}

// write - Writes a string as []byte to the connected serial port
func (conn *SerialConn) write(val string) (int, error) {
	if conn.port == nil {
		return 0, errors.Errorf(nilPortErrorf, conn.config.Name)
	}

	n, err := conn.port.Write([]byte(val + "\n"))
	if err != nil {
		return 0, errors.Wrapf(err, writeErrorf, conn.config.Name)
	}

	return n, nil
}

// readn - Reads n bytes from the connected serial port
func (conn *SerialConn) readn(n int, waitForOK bool) (string, error) {
	out := ""
	if conn.port == nil {
		return out, errors.Errorf(nilPortErrorf, conn.config.Name)
	}

	buf := make([]byte, n)
	loop := 1
	if waitForOK {
		loop = 10
	}

	for i := 0; i < loop; i++ {
		rb, err := conn.port.Read(buf)
		if err != nil && err != io.EOF {
			return "", errors.Wrapf(err, readErrorf, conn.config.Name)
		}
		if rb > 0 {
			if waitForOK {
				i--
			}
			out += string(buf[:rb])
			if isOK(out) {
				return out, nil
			} else if isERROR(out) {
				return "", errors.New(commandError)
			}
			if len(bytes.TrimSpace(buf)) == n {
				emptyBytes(buf)
			}
		}
	}

	return out, nil
}

// read - Reads up to 2048 bytes (at a time) from the connected serial port
func (conn *SerialConn) read(waitForOK bool) (string, error) {
	return conn.readn(2048, waitForOK)
}

// isOK - Returns strings.HasSuffix(val, "OK\r\n")
func isOK(val string) bool {
	return strings.HasSuffix(val, "OK\r\n")
}

// isERROR - Returns strings.HasSuffix(val, "ERROR\r\n")
func isERROR(val string) bool {
	return strings.HasSuffix(val, "ERROR\r\n")
}

// emptyBytes - Sets all values in a byte array to 0
func emptyBytes(buf []byte) {
	for i := range buf {
		buf[i] = 0
	}
}
