package sdk

import (
	"strconv"

	"github.com/pkg/errors"
)

// SMSMode - GSM SMS Mode (PDUMode, TextMode)
type SMSMode int

const (
	// PDUMode - Protocol Data Unit Mode (hex format)
	PDUMode SMSMode = 0
	// TextMode - Text Based Protocol
	TextMode SMSMode = 1
)

// Name - Returns the name of the SMSMode
func (m SMSMode) Name() string {
	switch m.Value() {
	case 0:
		return "PDUMode"
	case 1:
		return "TextMode"
	default:
		return "TextMode"
	}
}

// String - Convert SMSMode value to string
func (m SMSMode) String() string {
	return strconv.Itoa(m.Value())
}

// Value - Return SMSMode value
func (m SMSMode) Value() int {
	return int(m)
}

// MessageType - "ALL", "REC UNREAD", "REC READ", "STO UNSENT", "STO SENT"
type MessageType string

const (
	// MsgAll - All SMS Messages
	MsgAll MessageType = `ALL`
	// MsgRecUnread - Unread Incoming SMS Message
	MsgRecUnread MessageType = `REC UNREAD`
	// MsgRecRead - Read Incoming SMS Message
	MsgRecRead MessageType = `REC READ`
	// MsgStoUnsent - Unsent Outgoing SMS Message
	MsgStoUnsent MessageType = `STO UNSENT`
	// MsgStoSent - Sent Outgoing SMS Message
	MsgStoSent MessageType = `STO SENT`
)

func (mt MessageType) String() string {
	return string(mt)
}

// ParseMessageType - String => MessageType
func ParseMessageType(val string) (MessageType, error) {
	switch val {
	case MsgAll.String():
		return MsgAll, nil
	case MsgRecUnread.String():
		return MsgRecUnread, nil
	case MsgRecRead.String():
		return MsgRecRead, nil
	case MsgStoUnsent.String():
		return MsgStoUnsent, nil
	case MsgStoSent.String():
		return MsgStoSent, nil
	default:
		return "", errors.New("unsupported message type")
	}
}

// GSM SMS Message Format: +CMGR
// AT+CMGR=<index>
// +CMGR: <stat>, <oa>, [<alpha>], [<scts>]<CR><LF>
// <data><CR><LF><CR><LF>
// (OK || ERROR)<CR><LF>

// GSM SMS Message Format: +CMGL
// +CMGL: <index>, <stat>, <oa>, [<alpha>], [<scts>]<CR><LF>
// <data><CR><LF><CR><LF>
// ...
// ...
// (OK || ERROR)<CR><LF>

// Message - GSM SMS Message
type Message struct {
	Index                  int         // index
	Status                 MessageType // stat
	OriginatorAddr         string      // oa
	OriginatorName         string      // [alpha]
	ServiceCenterTimeStamp string      // [scts]
	Data                   string      // data
}
