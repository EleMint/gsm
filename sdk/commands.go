package sdk

// CommandType - "Test", "Read", "Set", "Execute"
type CommandType string

const (
	// Test - Used to check whether a command is supported by the Mobile Phone or Modem
	Test CommandType = "=?"
	// Read - Used to get Mobile Phone or Modem settings for an operation
	Read CommandType = "?"
	// Set - Used to modify Mobile Phone or Modem settings for an operation
	Set CommandType = "="
	// Execute - Used to carry out an operation against Mobile Phone or Modem
	Execute CommandType = ""
)

func (ct CommandType) String() string {
	return string(ct)
}

// Command - GSM Command (string)
type Command string

func (c Command) String() string {
	return string(c)
}

const (

	// Testing

	// AT - Check communication between Modem and Computer
	AT Command = "AT"

	// Call Controll

	// ATA - Answer command
	ATA Command = "ATA"
	// ATD - Dial
	ATD Command = "ATD"
	// ATH - Hang up
	ATH Command = "ATH"
	// ATL - Monitor speaker loudness
	ATL Command = "ATL"
	// ATM - Monitor speaker mode
	ATM Command = "ATM"
	// ATO - Go on-line
	ATO Command = "ATO"
	// ATP - Set pulse dial as default
	ATP Command = "ATP"
	// ATT - Set tone dial as default
	ATT Command = "ATT"
	// CSTA (AT+CSTA) - Select type of address
	CSTA Command = "AT+CSTA"
	// CRC (AT+CRC) - Cellular result codes
	CRC Command = "AT+CRC"

	// Data Card Control

	// ATI - Identification
	ATI Command = "ATI"
	// ATS - Select an S-register
	ATS Command = "ATS"
	// ATZ - Recall stored profile
	ATZ Command = "ATZ"
	// ATAF (AT&F) - Restore factory settings
	ATAF Command = "AT&F"
	// ATAV (AT&V) - View active configuration
	ATAV Command = "AT&V"
	// ATAW (AT&W) - Store parameters in given profile
	ATAW Command = "AT&W"
	// ATAY (AT&Y) - Select Set as power up option
	ATAY Command = "AT&Y"
	// CLCK (AT+CLCK) - Facility lock command
	CLCK Command = "AT+CLCK"
	// COLP (AT+COLP) - Connection line identification presentation
	COLP Command = "AT+COLP"
	// GCAP (AT+GCAP) - Request complete capabilities list
	GCAP Command = "AT+GCAP"
	// GMI (AT+GMI) - Request manufacturer identification
	GMI Command = "AT+GMI"
	// GMM (AT+GMM) - Request model identification
	GMM Command = "AT+GMM"
	// GMR (AT+GMR) - Request revision identification
	GMR Command = "AT+GMR"
	// GSN (AT+GSN) - Request product serial number identification (IMEI)
	GSN Command = "AT+GSN"

	// Phone Control

	// CBC (AT+CBC) - Battery charge
	CBC Command = "AT+CBC"
	// CGMI (AT+CGMI) - Request manufacturer identification
	CGMI Command = "AT+CGMI"
	// CGMM (AT+CGMM) - Request model identification
	CGMM Command = "AT+CGMM"
	// CGMR (AT+CGMR) - Request revision identification
	CGMR Command = "AT+CGMR"
	// CGSN (AT+CGSN) - Request product serial number identification
	CGSN Command = "AT+CGSN"
	// CMEE (AT+CMEE) - Report mobile equipment error
	CMEE Command = "AT+CMEE"
	// CPAS (AT+CPAS) - Phone activity status
	CPAS Command = "AT+CPAS"
	// CPBF (AT+CPBF) - Find phone book entries
	CPBF Command = "AT+CPBF"
	// CPBR (AT+CPBR) - Read phone book entry
	CPBR Command = "AT+CPBR"
	// CPBS (AT+CPBS) - Select phone book memory storage
	CPBS Command = "AT+CPBS"
	// CPBW (AT+CPBW) - Write phone book entry
	CPBW Command = "AT+CPBW"
	// CSCS (AT+CSCS) - Select TE character set
	CSCS Command = "AT+CSCS"
	// CSQ (AT+CSQ) - Signal quality
	CSQ Command = "AT+CSQ"

	// Computer Data Interface

	// ATE - Command Echo
	ATE Command = "ATE"
	// ATQ - Result code suppression
	ATQ Command = "ATQ"
	// ATV - Define response format
	ATV Command = "ATV"
	// ATX - Response range selection
	ATX Command = "ATX"
	// ATAC (AT&C) - Define DCD usage
	ATAC Command = "AT&C"
	// ATAD (AT&D) - Define DTR usage
	ATAD Command = "AT&D"
	// ATAK (AT&K) - Select flow control
	ATAK Command = "AT&K"
	// ATAQ (AT&Q) - Define communications mode option
	ATAQ Command = "AT&Q"
	// ATAS (AT&S) - Define DSR option
	ATAS Command = "AT&S"
	// ICF (AT+ICF) - DTE-DCE character framing
	ICF Command = "AT+ICF"
	// IFC (AT+IFC) - DTE-DCE Local flow control
	IFC Command = "AT+IFC"
	// IPR (AT+IPR) - Fixed DTE rate
	IPR Command = "AT+IPR"

	// Service

	// CLIP (AT+CLIP) - Calling line identification presentation
	CLIP Command = "AT+CLIP"
	// CR (AT+CR) - Service reporting control
	CR Command = "AT+CR"
	// DR (AT+DR) - Data compression reporting
	DR Command = "AT+DR"
	// ILRR (AT+ILRR) - DTE-DCE local rate reporting
	ILRR Command = "AT+ILRR"

	// Network Communication Parameter

	// ATB - Communication standard option
	ATB Command = "ATB"
	// CBST (AT+CBST) - Select bearer service type
	CBST Command = "AT+CBST"
	// CEER (AT+CEER) - Extended error report
	CEER Command = "AT+CEER"
	// CRLP (AT+CRLP) - Radio link protocol
	CRLP Command = "AT+CRLP"
	// DS (AT+DS) - Data compression
	DS Command = "AT+DS"

	// SMS Control

	// CSMS (AT+CSMS) - Select message service
	CSMS Command = "AT+CSMS"
	// CPMS (AT+CPMS) - Preferred message storage
	CPMS Command = "AT+CPMS"
	// CMGF (AT+CMGF) - Message format
	CMGF Command = "AT+CMGF"
	// CSCA (AT+CSCA) - Service center address
	CSCA Command = "AT+CSCA"
	// CSMP (AT+CSMP) - Set text mode parameters
	CSMP Command = "AT+CSMP"
	// CSDH (AT+CSDH) - Show text mode parameters
	CSDH Command = "AT+CSDH"
	// CSCB (AT+CSCB) - Select cell broadcast message types
	CSCB Command = "AT+CSCB"
	// CSAS (AT+CSAS) - Save settings
	CSAS Command = "AT+CSAS"
	// CRES (AT+CRES) - Restore settings
	CRES Command = "AT+CRES"
	// CNMI (AT+CNMI) - New message indications to TE
	CNMI Command = "AT+CNMI"
	// CMGL (AT+CMGL) - List messages
	CMGL Command = "AT+CMGL"
	// CMGR (AT+CMGR) - Read message
	CMGR Command = "AT+CMGR"
	// CMGS (AT+CMGS) - Send message
	CMGS Command = "AT+CMGS"
	// CMSS (AT+CMSS) - Send message from storage
	CMSS Command = "AT+CMSS"
	// CMGW (AT+CMGW) - Write message to memory
	CMGW Command = "AT+CMGW"
	// CMGD (AT+CMGD) - Delete message
	CMGD Command = "AT+CMGD"
)
